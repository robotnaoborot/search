from aiohttp import web
from search.config import TOPICS
from search.utils import parse_phrase, get_topics


async def search_handler(request):
    query = request.query.get('q', None)
    if not query:
        return web.json_response({'error': 'q parameter is required'}, status=400)
    topics = get_topics(query)
    return web.json_response({'topics': topics})


def init():
    for k in TOPICS:
        TOPICS[k] = [parse_phrase(phrase) for phrase in TOPICS[k] if isinstance(phrase, str)]


def main():
    init()
    app = web.Application()
    app.router.add_get('/', search_handler)
    web.run_app(app)


if __name__ == '__main__':
    main()
