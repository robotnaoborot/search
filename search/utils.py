import pymorphy2

from search.config import TOPICS

morph = pymorphy2.MorphAnalyzer()


def normalize(word):
    word = word.lower()
    try:
        return morph.parse(word)[0].normal_form
    except KeyError:
        return word


def parse_phrase(phrase):
    return set(map(normalize, phrase.lower().split()))


def get_topics(query):
    query = parse_phrase(query)
    if not query:
        return []
    topics = []
    for topic, phrases in TOPICS.items():
        for phrase in phrases:
            if query.issuperset(phrase):
                topics.append(topic)
    return topics
