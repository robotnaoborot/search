from search import __version__
from search.server import init
from search.utils import get_topics


def test_version():
    assert __version__ == '0.1.0'


def test_get_topics():
    init()
    topics = get_topics('рецепты блюд Тайской кухни')
    assert topics == ['кухня', 'товары']
    topics = get_topics('велосипед купить')
    assert topics == []

